# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

## [0.3.0] - 2023-12-09
### Added
- Object Pooling (using Unity's PoolSystem)
- Freelook Camera (Using InputSystem)
- Offscreen Target Indicator (with sample scene and assets)
- Simple State Machine (generic simple state machine)

### Changed
- refactored some of the extensions code


### Changed
- Refactored some code

## [0.1.2] - 2020-05-19
### Added
- This Changelog.
- Empty Editor and Playmode Tests.
- Sample Scene: Containing Examples on how to use the Library.

### Changed
- Restructured the project in order to be UnityPackages Complient.

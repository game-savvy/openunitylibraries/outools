using System;

namespace GameSavvy.OpenUnityTools.Attributes
{
    public class V2SlidersAttribute : Attribute
    {
        public float MinValue;
        public float MaxValue;

        public V2SlidersAttribute(float min, float max)
        {
            MinValue = min;
            MaxValue = max;
        }
    }
}

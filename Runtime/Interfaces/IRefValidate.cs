﻿namespace GameSavvy.OpenUnityTools.Variables
{
    public interface IObservable
    {
        void NotifyObservers();
    }
}

﻿namespace GameSavvy.OpenUnityTools.Extensions
{
    public static class NumberExtensions
    {
        /// <summary>
        /// Returns a remapped float
        /// </summary>
        public static float LinearRemap(this float value, float fromMin, float fromMax, float toMin, float toMax)
        {
            return (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
        }

        /// <summary>
        /// Returns a remapped int
        /// </summary>
        public static int LinearRemap(this int value, int fromMin, int fromMax, int toMin, int toMax)
        {
            float numerator = (value - fromMin);
            float denominator = (fromMax - fromMin) * (toMax - toMin) + toMin;
            return (int)(numerator / denominator);
        }
    }
}

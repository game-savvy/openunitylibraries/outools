using UnityEngine;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityTools.Extensions
{
    public class FieldOfViewController : MonoBehaviour
    {
        public float Radius = 5f;

        [Range(0f, 360f)]
        public float Angle = 90f;
        public Color WireColor = Color.green;
        public Color TargetColor = Color.red;

        public List<Transform> Targets { get; private set; }

        [SerializeField]
        private float _verticalGizmoOffset = 0.1f;
        public float VerticalGizmoOffset => _verticalGizmoOffset;

        [SerializeField]
        private LayerMask _targetsMask;

        [SerializeField]
        private LayerMask _obstacleMask;


        public void Update()
        {
            Targets = transform.GetObjectsInFovRaw<Transform>(Radius, Angle, _targetsMask, _obstacleMask, transform.forward);
        }

        private void OnDrawGizmosSelected()
        {
            if (Application.isPlaying) return;

            Targets = transform.GetObjectsInFovRaw<Transform>(Radius, Angle, _targetsMask, _obstacleMask, transform.forward);
        }

    }
}

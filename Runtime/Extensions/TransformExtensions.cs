using System.Collections.Generic;
using UnityEngine;

namespace GameSavvy.OpenUnityTools.Extensions
{
    public static class TransformExtensions
    {
        /// <summary>
        /// Destroy all children of this transform
        /// </summary>
        public static void DestroyChildren(this Transform transform)
        {
            foreach (Transform child in transform)
            {
                Object.Destroy(child.gameObject);
            }
        }

        /// <summary>
        /// Returns a list of visible Objects of type T, their angle and distance compared to the transform
        /// within the viewRadius from the transform position and
        /// within the viewAngle from the referenceDirection
        /// </summary>
        public static List<(T obj, float angle, float distance)> GetObjectsInFov<T>(this Transform transform,
                                                                                    float viewRadius,
                                                                                    float viewAngle,
                                                                                    LayerMask objectsMask,
                                                                                    LayerMask obstacleMask,
                                                                                    Vector3 referenceDirection)
        {
            if (transform == null) throw new UnityException("Error: transform must not be NULL");

            var collidersInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, objectsMask);
            var visibleObjects = new List<(T, float, float)>(collidersInViewRadius.Length);

            for (int i = 0; i < collidersInViewRadius.Length; i++)
            {
                var comp = collidersInViewRadius[i].GetComponent<T>();
                if (comp == null) continue;

                var target = collidersInViewRadius[i].transform;
                var dirToTarget = (target.position - transform.position).normalized;

                float angle = Vector3.Angle(referenceDirection, dirToTarget);
                if (angle > viewAngle / 2f) continue;

                float distanceToTarget = Vector3.Distance(transform.position, target.position);
                if (Physics.Raycast(transform.position, dirToTarget, distanceToTarget, obstacleMask) == false)
                {
                    visibleObjects.Add((comp, angle, distanceToTarget));
                }
            }

            return visibleObjects;
        }

        /// <summary>
        /// Returns a list of visible Objects of type T that are
        /// within the viewRadius from the transform position and
        /// within the viewAngle from the referenceDirection
        /// </summary>
        public static List<T> GetObjectsInFovRaw<T>(this Transform transform,
                                                    float viewRadius,
                                                    float viewAngle,
                                                    LayerMask objectsMask,
                                                    LayerMask obstacleMask,
                                                    Vector3 referenceDirection)
        {
            if (transform == null) throw new UnityException("Error: transform must not be NULL");

            var collidersInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, objectsMask);
            var visibleObjects = new List<T>(collidersInViewRadius.Length);

            for (int i = 0; i < collidersInViewRadius.Length; i++)
            {
                var comp = collidersInViewRadius[i].GetComponent<T>();
                if (comp == null) continue;

                var target = collidersInViewRadius[i].transform;
                var dirToTarget = (target.position - transform.position).normalized;

                float angle = Vector3.Angle(referenceDirection, dirToTarget);
                if (angle > viewAngle / 2f) continue;

                float distanceToTarget = Vector3.Distance(transform.position, target.position);
                if (Physics.Raycast(transform.position, dirToTarget, distanceToTarget, obstacleMask) == false)
                {
                    visibleObjects.Add(comp);
                }
            }

            return visibleObjects;
        }

        /// <summary>
        /// Returns a horizontal direction Vector based on the angleInDegrees (X, 0f, Z)
        /// If globalAngle = true, then the angle is taken from Global World Forward
        /// </summary>
        public static Vector3 DirFromAngle(this Transform transform, float angleInDegrees, bool globalAngle = false)
        {
            if (transform == null) throw new UnityException("Error: transform must not be NULL");

            if (globalAngle == false) angleInDegrees += transform.eulerAngles.y;

            return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0f, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        }
    }
}
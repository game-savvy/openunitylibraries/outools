﻿using UnityEngine;

namespace GameSavvy.OpenUnityTools.Extensions
{
    public static class GameObjectExtensions
    {
        /// <summary>
        /// Returns a component after either finding it on the game object or otherwise adding a component to it
        /// </summary>
        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            if (gameObject == null) throw new UnityEngine.UnityException("Cannot Get or Add a component to a NULL Object");

            if (gameObject.TryGetComponent<T>(out T requestedComponent))
            {
                return requestedComponent;
            }

            return gameObject.AddComponent<T>();
        }
    }
}

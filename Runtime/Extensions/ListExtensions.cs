﻿using System.Collections.Generic;

namespace GameSavvy.OpenUnityTools.Extensions
{
    public static class ListExtensions
    {
        /// <summary>
        /// Returns a random item from the list
        /// </summary>
        public static T Random<T>(this IList<T> list, int? seed = null)
        {
            if (list?.Count == 0) return default(T);
            if (seed != null) UnityEngine.Random.InitState(seed.Value);

            return list[UnityEngine.Random.Range(0, list.Count)];
        }

        /// <summary>
        /// Remove a random item from the list and returns it
        /// </summary>
        public static T RemoveRandom<T>(this IList<T> list, int? seed = null)
        {
            if (list?.Count == 0) return default(T);
            if (seed != null) UnityEngine.Random.InitState(seed.Value);

            int indexToRemoveAt = UnityEngine.Random.Range(0, list.Count);
            var item = list[indexToRemoveAt];
            list.RemoveAt(indexToRemoveAt);
            return item;
        }

        /// <summary>
        /// Shuffles the items of the list
        /// </summary>
        public static void Shuffle<T>(this IList<T> list, int? seed = null)
        {
            if (list == null || list.Count == 0 || list.Count == 1) return;

            System.Random rng;
            if (seed == null)
            {
                rng = new();
            }
            else
            {
                rng = new(seed.Value);
            }

            for (int i = list.Count - 1; i > 1; i--)
            {
                int k = rng.Next(i);
                (list[k], list[i]) = (list[i], list[k]);
            }
        }
    }
}
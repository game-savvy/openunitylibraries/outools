﻿using UnityEngine;

namespace GameSavvy.OpenUnityTools.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Returns the RTF text with Color
        /// </summary>
        public static string ToColor(this string text, Color color)
        {
            return Color(text, color);
        }

        /// <summary>
        /// Returns the RTF text ITALIC
        /// </summary>
        public static string ToItalic(this string text)
        {
            return Italic(text);
        }

        /// <summary>
        /// Returns the RTF text BOLD
        /// </summary>
        public static string ToBold(this string text)
        {
            return Bold(text);
        }

        /// <summary>
        /// Returns the RTF text with a new Size
        /// </summary>
        public static string ToSize(this string text, int size)
        {
            return Size(text, size);
        }


        /// <summary>
        /// Returns the RTF text with Color
        /// </summary>
        public static string Color(string text, Color color)
        {
            return $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>{text}</color>";
        }

        /// <summary>
        /// Returns the RTF text ITALIC
        /// </summary>
        public static string Italic(string text)
        {
            return $"<i>{text}</i>";
        }

        /// <summary>
        /// Returns the RTF text BOLD
        /// </summary>
        public static string Bold(string text)
        {
            return $"<b>{text}</b>";
        }

        /// <summary>
        /// Returns the RTF text with a new Size
        /// </summary>
        public static string Size(string text, int size)
        {
            return $"<size={size}>{text}</size>";
        }

    }
}

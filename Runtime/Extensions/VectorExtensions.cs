﻿using UnityEngine;

namespace GameSavvy.OpenUnityTools.Extensions
{
    public static class VectorExtensions
    {
        /// <summary>
        /// Returns a copy of this vector with an altered X and/or Y and/or Z components
        /// </summary>
        public static Vector3 With(this Vector3 original, float? x = null, float? y = null, float? z = null)
        {
            return new Vector3(x ?? original.x, y ?? original.y, z ?? original.z);
        }

        /// <summary>
        /// Returns a copy of this vector with a value added to either the X and/or Y and/or Z components
        /// </summary>
        public static Vector3 AddTo(this Vector3 original, float x = 0, float y = 0, float z = 0)
        {
            return new Vector3(original.x + x, original.y + y, original.z + z);
        }

        /// <summary>
        /// Returns a Vector2 representation of the vector with only the X and Y components
        /// Vector2(original.x, original.y);
        /// </summary>
        public static Vector2 ToXYV2(this Vector3 original)
        {
            return new Vector2(original.x, original.y);
        }

        /// <summary>
        /// Returns a Vector2 representation of the vector with only the X and Z components
        /// Vector2(original.x, original.z);
        /// </summary>
        public static Vector2 ToXZV2(this Vector3 original)
        {
            return new Vector2(original.x, original.z);
        }

        /// <summary>
        /// Returns the nearest point on the Axis to the provided point
        /// </summary>
        public static Vector3 NearestPointOnAxis(this Vector3 axisDirection, Vector3 point, bool isNormalized = false)
        {
            if (!isNormalized) axisDirection.Normalize();
            var d = Vector3.Dot(point, axisDirection);
            return axisDirection * d;
        }

        /// <summary>
        /// Returns the nearest point on the Line to the provided point
        /// </summary>
        public static Vector3 NearestPointOnLine(this Vector3 lineDirection, Vector3 point, Vector3 pointOnLine, bool isNormalized = false)
        {
            if (!isNormalized) lineDirection.Normalize();
            var d = Vector3.Dot(point - pointOnLine, lineDirection);
            return pointOnLine + (lineDirection * d);
        }

        //----------------------------------------------------------------------------

        /// <summary>
        /// Returns a copy of this vector with an altered X and/or Y components
        /// </summary>
        public static Vector2 With(this Vector2 original, float? x = null, float? y = null)
        {
            return new Vector2(x ?? original.x, y ?? original.y);
        }

        /// <summary>
        /// Returns a copy of this vector with a value added to either the X and/or Y components
        /// </summary>
        public static Vector2 AddTo(this Vector2 original, float x = 0, float y = 0)
        {
            return new Vector2(original.x + x, original.y + y);
        }

        /// <summary>
        /// Returns a Vector3 representation of the vector with only the X and Y components, Z = 0
        /// Vector3(original.x, original.y, 0f);
        /// </summary>
        public static Vector3 ToXYV3(this Vector2 original)
        {
            return new Vector3(original.x, original.y, 0f);
        }

        /// <summary>
        /// Returns a Vector3 representation of the vector with only the X and Z components, Y = 0
        /// Vector3(original.x, 0f, original.y);
        /// </summary>
        public static Vector3 ToXZV3(this Vector2 original)
        {
            return new Vector3(original.x, 0f, original.y);
        }
    }
}
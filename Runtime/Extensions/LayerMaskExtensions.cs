using UnityEngine;

namespace GameSavvy.OpenUnityTools.Extensions
{
    public static class LayerMaskExtensions
    {
        /// <summary>
        /// Returns true if layerValue is contained within the layerMask, false otherwise
        /// </summary>
        /// <returns></returns>
        public static bool ContainsLayer(this LayerMask layerMask, int layerValue)
        {
            return ((int)(Mathf.Pow(2, layerValue)) & layerMask.value) != 0;
        }

        /// <summary>
        /// Remember that when you ask gameObject.paramref name="layerIndex", this returs the index, not the value.
        /// hence why you need to use this function to get the value and compare against a layerMask
        /// </summary>
        /// <param name="layerIndex">Usually get this by using, gameObeject.layer</param>
        /// <returns></returns>
        public static int GetLayerValue(this int layerIndex)
        {
            return (int)(Mathf.Pow(2, layerIndex));
        }
    }
}

namespace GameSavvy.OpenUnityTools.SimpleFSM
{
    /// <summary>
    /// Simple Generic State, to be inherited from and used with the SimpleStateMachine
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SimpleState<T>
    {
        protected T _context;
        public T Context => _context;

        protected SimpleStateMachine<T> _stateMachine;
        public SimpleStateMachine<T> StateMachine => _stateMachine;

        protected SimpleState(T context, SimpleStateMachine<T> stateMachine)
        {
            _context = context;
            _stateMachine = stateMachine;
        }

        public abstract void OnEnter();
        public abstract void OnUpdate(float deltaTime);
        public abstract void OnLateUpdate(float deltaTime);
        public abstract void OnFixedUpdate(float fixedDeltaTime);
        public abstract void OnExit();
    }
}
namespace GameSavvy.OpenUnityTools.SimpleFSM
{
    /// <summary>
    /// Simple Generic State Machine.  
    /// This State Machine is designed to have a reference to the 'Context' it is being used with.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SimpleStateMachine<T>
    {
        private SimpleState<T> _currentState;
        public SimpleState<T> CurrentState => _currentState;

        private T _context;
        public T Context => _context;

        public SimpleStateMachine(T context)
        {
            _context = context;
        }

        /// <summary>
        /// Changes the current state to the provided state, if the provided state is null, nothing happens
        /// </summary>
        /// <param name="newState"></param>
        public void ChangeState(SimpleState<T> newState)
        {
            if (newState == null) return;

            _currentState?.OnExit();
            _currentState = newState;
            _currentState?.OnEnter();
        }

        public void OnUpdate(float deltaTime) => _currentState?.OnUpdate(deltaTime);
        public void OnLateUpdate(float deltaTime) => _currentState?.OnLateUpdate(deltaTime);
        public void OnFixedUpdate(float fixedDeltaTime) => _currentState?.OnFixedUpdate(fixedDeltaTime);
    }
}
using GameSavvy.OpenUnityTools.Extensions;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GameSavvy.OpenUnityTools.FreelookCameras
{
    public class FreelookCamera : MonoBehaviour
    {
        [Space(10)]
        [SerializeField]
        private float _mouseSensitivity = 0.4f;

        [Space(10)]
        [Header("3D Movement")]
        [SerializeField]
        private float _moveSpeed = 6f;

        [SerializeField]
        private float _fastMoveSpeed = 12;


        [Space(10)]
        [Header("2D Panning")]
        [SerializeField]
        private bool _invertPanning;


        private FreelookCameraInputActions _inputActions;
        private InputAction _enable3dMovementButton;
        private InputAction _moveInputV3;
        private InputAction _moveFastButton;
        private InputAction _lookInputV2;
        private InputAction _panning2DButton;
        private InputAction _speedModifierAxis;


        private void Awake()
        {
            _inputActions = new();
            _enable3dMovementButton = _inputActions.FreelookCamera.EnableMovement;
            _moveInputV3 = _inputActions.FreelookCamera.Move;
            _moveFastButton = _inputActions.FreelookCamera.MoveFast;
            _lookInputV2 = _inputActions.FreelookCamera.Look;
            _speedModifierAxis = _inputActions.FreelookCamera.SpeedModifier;
            _panning2DButton = _inputActions.FreelookCamera.Panning2DButton;
        }

        private void OnEnable()
        {
            _inputActions.Enable();
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void LateUpdate()
        {
            _fastMoveSpeed += Time.deltaTime * 3f * _speedModifierAxis.ReadValue<float>();
            var moveSpeed = _moveFastButton.IsPressed() ? _fastMoveSpeed : _moveSpeed;

            if (_panning2DButton.IsPressed())
            {
                Cursor.lockState = CursorLockMode.None;
                moveSpeed *= _invertPanning ? 1f : -1f;
                transform.Translate(moveSpeed * Time.deltaTime * Mouse.current.delta.ReadValue());
                return;
            }

            if (_enable3dMovementButton.IsPressed() == false) return;

            Cursor.lockState = CursorLockMode.Locked;

            var moveInput = Time.deltaTime * _moveInputV3.ReadValue<Vector3>();
            var lookInput = _mouseSensitivity * _lookInputV2.ReadValue<Vector2>();
            var moveVector = moveSpeed * moveInput;

            transform.Translate(moveVector.With(y: 0f), Space.Self);
            transform.Translate(moveVector.y * Vector3.up, Space.World);
            transform.Rotate(0f, lookInput.x, 0f, Space.World);
            transform.Rotate(-lookInput.y, 0f, 0f, Space.Self);
        }

        private void OnDisable()
        {
            _enable3dMovementButton.Disable();
            _moveInputV3.Disable();
            _moveFastButton.Disable();
            _lookInputV2.Disable();
            _speedModifierAxis.Disable();
            _panning2DButton.Disable();

            Cursor.lockState = CursorLockMode.None;
        }
    }
}
namespace GameSavvy.OpenUnityTools.FastFSM
{
    public interface IStateMachine<T>
    {
        T Context { get; }
        IState<T> CurrentState { get; }
        IStatesContainer<T> AvailableStates { get; }
        string CurrentStateName => CurrentState?.StateName;

        void Init(T context, IState<T> initialState, IStatesContainer<T> statesContainer);

        /// <summary>
        /// Changes the current state to the provided state, if the provided state is null, nothing happens.
        /// </summary>
        void ChangeState(IState<T> newState);

        void OnEnter() => CurrentState?.OnEnter(this);
        void OnUpdate(float deltaTime) => CurrentState?.OnUpdate(this, deltaTime);
        void OnLateUpdate(float deltaTime) => CurrentState?.OnLateUpdate(this, deltaTime);
        void OnFixedUpdate(float fixedDeltaTime) => CurrentState?.OnFixedUpdate(this, fixedDeltaTime);
        void OnExit() => CurrentState?.OnExit(this);

        IState<T> GetState<TS>() where TS : IState<T> => AvailableStates.GetState<TS>();
        void AddState<TS>(TS state) where TS : IState<T> => AvailableStates.AddState(state);
        void RemoveState<TS>() where TS : IState<T> => AvailableStates.RemoveState<TS>();
        void RemoveState<TS>(TS state) where TS : IState<T> => AvailableStates.RemoveState(state);
        void ClearStates() => AvailableStates.ClearStates();
        string GetAvailableStatesNames() => AvailableStates.GetAvailableStatesNames();
    }
}
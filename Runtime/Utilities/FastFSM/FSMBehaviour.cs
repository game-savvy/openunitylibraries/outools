using UnityEngine;

namespace GameSavvy.OpenUnityTools.FastFSM
{
    /// <summary>
    /// A MonoBehaviour version of the FSM, designed to have a reference to the 'Context' it is being used with.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FSMBehaviour<T> : MonoBehaviour, IStateMachine<T>
    {
        public T Context { get; protected set; }
        public IState<T> CurrentState { get; protected set; }
        public IStatesContainer<T> AvailableStates { get; protected set; }

        public void Init(T context, IState<T> initialState, IStatesContainer<T> statesContainer)
        {
            Context = context;
            CurrentState = initialState;
            AvailableStates = statesContainer;
            CurrentState?.OnEnter(this);
        }

        public void ChangeState(IState<T> newState)
        {
            if (newState == null) return;

            CurrentState?.OnExit(this);
            CurrentState = newState;
            CurrentState?.OnEnter(this);
        }
    }
}
namespace GameSavvy.OpenUnityTools.FastFSM
{
    /// <summary>
    ///  Generic State, to be inherited from and used with the StateMachine
    /// To be used with Raw Classes and not MonoBehaviours.
    /// </summary>
    /// <typeparam name="T">The type of the Context to be used with</typeparam>
    public abstract class AStateClass<T> : IState<T>
    {
        public abstract void Init(IStateMachine<T> stateMachine);

        public abstract void OnEnter(IStateMachine<T> stateMachine);
        public abstract void OnUpdate(IStateMachine<T> stateMachine, float deltaTime);
        public abstract void OnLateUpdate(IStateMachine<T> stateMachine, float deltaTime);
        public abstract void OnFixedUpdate(IStateMachine<T> stateMachine, float fixedDeltaTime);
        public abstract void OnExit(IStateMachine<T> stateMachine);
    }
}
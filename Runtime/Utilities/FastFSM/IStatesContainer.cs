namespace GameSavvy.OpenUnityTools.FastFSM
{
    public interface IStatesContainer<T>
    {
        IState<T> GetState<TS>() where TS : IState<T>;
        void AddState<TS>(TS state) where TS : IState<T>;
        void RemoveState<TS>() where TS : IState<T>;
        void RemoveState<TS>(TS state) where TS : IState<T>;
        void ClearStates();
        string GetAvailableStatesNames();
    }
}
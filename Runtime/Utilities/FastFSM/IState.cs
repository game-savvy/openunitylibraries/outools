namespace GameSavvy.OpenUnityTools.FastFSM
{
    public interface IState<T>
    {
        public string StateName => GetType().Name;

        public void Init(IStateMachine<T> stateMachine);

        public void OnEnter(IStateMachine<T> stateMachine);
        public void OnUpdate(IStateMachine<T> stateMachine, float deltaTime);
        public void OnLateUpdate(IStateMachine<T> stateMachine, float deltaTime);
        public void OnFixedUpdate(IStateMachine<T> stateMachine, float fixedDeltaTime);
        public void OnExit(IStateMachine<T> stateMachine);
    }
}
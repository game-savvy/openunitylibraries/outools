using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameSavvy.OpenUnityTools.FastFSM
{
    public class StatesDictionary<T> : IStatesContainer<T>
    {
        Dictionary<Type, IState<T>> AvailableStates { get; }

        public IState<T> GetState<TS>() where TS : IState<T>
        {
            bool hasValue = AvailableStates.TryGetValue(typeof(TS), out var state);
            return hasValue ? state : default; // _TODO: Check if this is correct
        }

        public void AddState<TS>(TS state) where TS : IState<T>
        {
            Type type = typeof(TS);
            if (AvailableStates.ContainsKey(type))
            {
                Debug.LogWarning($"State of type {type} already exists, replacing it.");
                AvailableStates[type] = state;
            }
            else
            {
                AvailableStates.Add(type, state);
            }
        }

        public void RemoveState<TS>() where TS : IState<T>
        {
            Type type = typeof(TS);
            if (AvailableStates.ContainsKey(type))
            {
                AvailableStates.Remove(type);
            }
            else
            {
                Debug.LogWarning($"State of type {type} does not exist, cannot remove it.");
            }
        }

        public void RemoveState<TS>(TS state) where TS : IState<T>
        {
            Type type = typeof(TS);
            if (AvailableStates.ContainsKey(type))
            {
                AvailableStates.Remove(type);
            }
            else
            {
                Debug.LogWarning($"State of type {type} does not exist, cannot remove it.");
            }
        }

        public void ClearStates()
        {
            AvailableStates.Clear();
        }

        public string GetAvailableStatesNames()
        {
            var names = "";
            foreach (var state in AvailableStates)
            {
                names += state.Key.Name + ", ";
            }
            return names;
        }
    }
}
namespace GameSavvy.OpenUnityTools.FastFSM
{
    /// <summary>
    /// Generic State Machine, designed to have a reference to the 'Context' it is being used with.
    /// </summary>
    /// <typeparam name="T">The Context Type, this is usually the "Owner" of the FSM, but it can also be a Data Instance to be acted on.</typeparam>
    public class FSM<T> : IStateMachine<T>
    {
        public T Context { get; protected set; }
        public IState<T> CurrentState { get; protected set; }
        public IStatesContainer<T> AvailableStates { get; protected set; }

        public FSM(T context, IState<T> initialState, IStatesContainer<T> statesContainer)
        {
            Init(context, initialState, statesContainer);
        }

        public void Init(T context, IState<T> initialState, IStatesContainer<T> statesContainer)
        {
            Context = context;
            CurrentState = initialState;
            AvailableStates = statesContainer;
            CurrentState?.OnEnter(this);
        }

        public void ChangeState(IState<T> newState)
        {
            if (newState == null) return;

            CurrentState?.OnExit(this);
            CurrentState = newState;
            CurrentState?.OnEnter(this);
        }
    }
}
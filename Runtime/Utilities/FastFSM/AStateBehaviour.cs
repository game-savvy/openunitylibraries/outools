using UnityEngine;

namespace GameSavvy.OpenUnityTools.FastFSM
{
    /// <summary>
    /// Generic State, to be inherited from and used with the StateMachine
    /// To be used with MonoBehaviours and not Raw Classes.
    /// </summary>
    /// <typeparam name="T">The type of the Context to be used with</typeparam>
    public abstract class AStateBehaviour<T> : MonoBehaviour, IState<T>
    {
        public abstract void Init(IStateMachine<T> stateMachine);

        public abstract void OnEnter(IStateMachine<T> stateMachine);
        public abstract void OnUpdate(IStateMachine<T> stateMachine, float deltaTime);
        public abstract void OnLateUpdate(IStateMachine<T> stateMachine, float deltaTime);
        public abstract void OnFixedUpdate(IStateMachine<T> stateMachine, float fixedDeltaTime);
        public abstract void OnExit(IStateMachine<T> stateMachine);
    }
}
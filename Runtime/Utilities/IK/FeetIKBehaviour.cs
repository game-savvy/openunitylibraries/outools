﻿using System;
using GameSavvy.OpenUnityTools.Extensions;
using UnityEngine;

namespace GameSavvy.OpenUnityTools.IK
{
    [RequireComponent(typeof(Animator))]
    public class FeetIKBehaviour : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)]
        private float _IKPositionWeight = 1f;

        [SerializeField, Range(0f, 1f)]
        private float _IKRotationWeight = 0.85f;

        [SerializeField]
        private LayerMask _RaycastLayerMask = 0xFFFF;

        [SerializeField]
        private float _RaycastOffsetY = 0.5f;

        [SerializeField]
        private float _FeetPosOffsetY = 0.055f;

        [SerializeField]
        private float _LerpFactor = 18f;

        [SerializeField]
        private Vector3 _KneeHintOffset = Vector3.zero;

        private Animator _Anim;
        private Transform _LeftFoot;
        private Transform _RightFoot;

        private Vector3 _LeftFootIKPos;
        private Vector3 _RightFootIKPos;
        private Quaternion _LeftFootIKRot;
        private Quaternion _RightFootIKRot;

        private Vector3 _LeftFootLastPos;
        private Vector3 _RightFootLastPos;
        private Quaternion _LeftFootLastRot;
        private Quaternion _RightFootLastRot;

        private float _LeftMultiplier = -1f;
        private float _RightMultiplier = -1f;
        private float _LeftLerpValue = 0f;
        private float _RightLerpValue = 0f;


        private void Awake()
        {
            _Anim = GetComponent<Animator>();
            _LeftFoot = _Anim.GetBoneTransform(HumanBodyBones.LeftFoot);
            _RightFoot = _Anim.GetBoneTransform(HumanBodyBones.RightFoot);
        }

        private void OnEnable()
        {
            _LeftFootIKRot = _LeftFoot.rotation;
            _RightFootIKRot = _RightFoot.rotation;
        }

        private void LateUpdate()
        {
            _LeftLerpValue = Mathf.Clamp01(_LeftLerpValue + (_LeftMultiplier * Time.deltaTime * _LerpFactor));
            _RightLerpValue = Mathf.Clamp01(_RightLerpValue + (_RightMultiplier * Time.deltaTime * _LerpFactor));
            _LeftFootLastPos = _LeftFoot.position;
            _RightFootLastPos = _RightFoot.position;
            _LeftFootLastRot = _LeftFoot.rotation;
            _RightFootLastRot = _RightFoot.rotation;
        }

        private void OnAnimatorIK()
        {
            SolveFootIK(_LeftFoot, ref _LeftFootIKPos, ref _LeftFootIKRot, SetLeftIKWeights);
            SolveFootIK(_RightFoot, ref _RightFootIKPos, ref _RightFootIKRot, SetRightIKWeights);
            LerpFootIKs();

            SetLeftIKPosRot();
            SetRightIKPosRot();
        }

        private void SolveFootIK(Transform foot, ref Vector3 footPos, ref Quaternion footRot, Action<float> SetIKWeights)
        {
            var rayStart = foot.position.AddTo(y: _RaycastOffsetY);
            float ikWeight = -1f;
            if (Physics.Raycast(rayStart, Vector3.down, out var hit, _RaycastOffsetY + _FeetPosOffsetY, _RaycastLayerMask))
            {
                if (hit.point.y > foot.position.y - _FeetPosOffsetY)
                {
                    footPos = hit.point.AddTo(y: _FeetPosOffsetY);
                    footRot = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
                    ikWeight = 1f;
                }
            }
            SetIKWeights(ikWeight);
        }

        private void LerpFootIKs()
        {
            var lerpVal = _LerpFactor * Time.deltaTime;
            _LeftFootIKPos = Vector3.Lerp(_LeftFootLastPos, _LeftFootIKPos, lerpVal);
            _RightFootIKPos = Vector3.Lerp(_RightFootLastPos, _RightFootIKPos, lerpVal);
        }

        private void SetLeftIKWeights(float lerpMult = 1f)
        {
            _LeftMultiplier = lerpMult;
            var posW = _LeftLerpValue * _IKPositionWeight;
            var rotW = _LeftLerpValue * _IKRotationWeight;
            _Anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, posW);
            _Anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, rotW);
            _Anim.SetIKHintPositionWeight(AvatarIKHint.LeftKnee, posW);
        }

        private void SetRightIKWeights(float lerpMult = 1f)
        {
            _RightMultiplier = lerpMult;
            var posW = _RightLerpValue * _IKPositionWeight;
            var rotW = _RightLerpValue * _IKRotationWeight;
            _Anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, posW);
            _Anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, rotW);
            _Anim.SetIKHintPositionWeight(AvatarIKHint.RightKnee, posW);
        }

        private void SetLeftIKPosRot()
        {
            _Anim.SetIKPosition(AvatarIKGoal.LeftFoot, _LeftFootIKPos);
            _Anim.SetIKRotation(AvatarIKGoal.LeftFoot, _LeftFootIKRot);
            _Anim.SetIKHintPosition
            (
                AvatarIKHint.LeftKnee,
                transform.position + transform.TransformVector(_KneeHintOffset.With(x: -_KneeHintOffset.x))
            );
        }

        private void SetRightIKPosRot()
        {
            _Anim.SetIKPosition(AvatarIKGoal.RightFoot, _RightFootIKPos);
            _Anim.SetIKRotation(AvatarIKGoal.RightFoot, _RightFootIKRot);
            _Anim.SetIKHintPosition
            (
                AvatarIKHint.RightKnee,
                transform.position + transform.TransformVector(_KneeHintOffset)
            );
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(transform.position + transform.TransformVector(_KneeHintOffset), 0.1f);
            Gizmos.DrawWireSphere(transform.position + transform.TransformVector(_KneeHintOffset.With(x: -_KneeHintOffset.x)), 0.1f);
        }

    }
}

﻿using UnityEngine;
namespace GameSavvy.OpenUnityTools.IK
{
    [RequireComponent(typeof(Animator))]
    public class LookIKBehaviour : MonoBehaviour
    {
        public Transform LookAtTarget;

        [SerializeField, Range(0f, 1f)]
        private float _LookIKWeight = 1f;

        [SerializeField, Range(0f, 1f)]
        private float _BodyIKWeight = 0.6f;

        [SerializeField, Range(0f, 1f)]
        private float _HeadIKWeight = 0.75f;

        [SerializeField, Range(0f, 1f)]
        private float _EyesIKWeight = 0.9f;

        [SerializeField, Range(0f, 1f)]
        private float _ClampIKWeight = 0.5f;

        [SerializeField]
        private float _LerpFactor = 9f;

        private Vector3 _LookAtPos;
        private Animator _Anim;
        private float _LerpValue;
        private float _Multiplier = 1f;


        private void Awake()
        {
            _Anim = GetComponent<Animator>();
        }

        private void Update()
        {
            if (LookAtTarget == null)
            {
                _Multiplier = -1f;
                return;
            }

            _Multiplier = 1f;
            _LookAtPos = Vector3.Lerp(_LookAtPos, LookAtTarget.position, _LerpFactor * Time.deltaTime);
        }

        private void OnAnimatorIK()
        {
            if (_Multiplier == 1f && _LerpValue == 0f) _LookAtPos = transform.position + (transform.forward * 10f) + (transform.up * 1.5f);
            _LerpValue = Mathf.Clamp01(_LerpValue + (_Multiplier * Time.deltaTime * _LerpFactor));
            _Anim.SetLookAtWeight(_LookIKWeight * _LerpValue, _BodyIKWeight, _HeadIKWeight, _EyesIKWeight, _ClampIKWeight);
            _Anim.SetLookAtPosition(_LookAtPos);
        }
    }
}

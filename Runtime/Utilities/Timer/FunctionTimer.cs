using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameSavvy.OpenUnityTools.Timers
{
    public class FunctionTimer
    {
    #region Statics

        internal static List<FunctionTimer> ExistingTimers = new();

        private static FunctionTimerController _timerController;

        private static void CreateObjectIfNeeded()
        {
            if (_timerController != null) return;

            _timerController = new GameObject("__FunctionTimerController__",
                                              typeof(FunctionTimerController))
               .GetComponent<FunctionTimerController>();
        }

        public static FunctionTimer Create(Action<int> action, float duration, int loops = 1, bool active = true)
        {
            CreateObjectIfNeeded();
            var timer = new FunctionTimer(action, duration, loops, active);
            ExistingTimers.Add(timer);
            return timer;
        }

    #endregion Statics

        private readonly bool _autoDestroy;
        private readonly int _desiredLoops;
        private readonly float _duration;

        private Action<int> _action;

        private float _currentTime;
        public float CurrentTime => _currentTime;
        public float TimeLeft => _duration - _currentTime;

        private int _loops;
        public int Loops => _loops;

        private bool _isActive;
        public bool IsActive => _isActive;

        public bool ShouldDestroy { get; private set; }
        public float Percent => _currentTime / _duration;

        private FunctionTimer(Action<int> action, float duration, int loops = 1, bool active = true, bool autoDestroy = false)
        {
            _autoDestroy = autoDestroy;
            _isActive = active;
            _duration = Mathf.Abs(duration);
            _action = action;
            _desiredLoops = loops;
            ShouldDestroy = false;
            Reset();
        }

        internal void TimerTick(float deltaTime)
        {
            if (_isActive == false) return;

            _currentTime += deltaTime;
            if (_currentTime < _duration) return;

            _action?.Invoke(_loops);

            _loops++;
            if (_loops == _desiredLoops)
            {
                Pause();

                if (_autoDestroy)
                {
                    Destroy();
                    return;
                }

                _currentTime = _duration;
            }
            else
            {
                _currentTime -= _duration;
                if (_loops < 0) _loops = -1;
            }
        }


        public void Pause() => _isActive = false;
        public void Resume() => _isActive = true;

        private void Reset()
        {
            _loops = 0;
            _currentTime = 0f;
        }

        public void Stop()
        {
            Pause();
            Reset();
        }

        public void Restart()
        {
            Reset();
            Resume();
        }

        public void Destroy()
        {
            _isActive = false;
            _action = null;
            ShouldDestroy = true;
        }
    }
}
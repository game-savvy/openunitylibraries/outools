using UnityEngine;
using UnityEngine.InputSystem;

namespace GameSavvy.OpenUnityTools.Timers.Tester
{
    public class FunctionTimerTester : MonoBehaviour
    {
        private FunctionTimer _timer1;
        private FunctionTimer _timer2;
        private FunctionTimer _timer3;
        private float _startTime;

        private void OnEnable()
        {
            _timer1 = FunctionTimer.Create((l) => Debug.Log($"[{Time.time}, {l}] Timer 1"), 1f, -1, false);
            _timer2 = FunctionTimer.Create((l) => Debug.Log($"[{Time.time - _startTime}, {l}] Timer 2"), Time.deltaTime * 2f, 3);
            _timer3 = FunctionTimer.Create((l) => Debug.Log($"[{Time.time - _startTime}, {l}] Timer 3"), 4f);
            _startTime = Time.time;
            Debug.Log($"Start [{_startTime}]");
        }

        private void Update()
        {
            if (Keyboard.current.digit1Key.wasPressedThisFrame)
            {
                Debug.Log($"RESUME [{Time.time - _startTime}]");
                _timer1.Resume();
            }
            if (Keyboard.current.digit2Key.wasPressedThisFrame)
            {
                Debug.Log($"PAUSE [{Time.time - _startTime}]");
                _timer1.Pause();
            }
            if (Keyboard.current.digit3Key.wasPressedThisFrame)
            {
                Debug.Log($"STOP [{Time.time - _startTime}]");
                _timer1.Stop();
            }
        }

        private void OnGUI()
        {
            GUILayout.Label($"Timer 1 :: {_timer1.CurrentTime.ToString("0.000")} - {_timer1.Loops} :: {(_timer1.Percent * 100f).ToString("0.0")}%");
            GUILayout.Label($"Timer 2 :: {_timer2.CurrentTime.ToString("0.000")} - {_timer2.Loops} :: {(_timer2.Percent * 100f).ToString("0.0")}%");
            GUILayout.Label($"Timer 3 :: {_timer3.CurrentTime.ToString("0.000")} - {_timer3.Loops} :: {(_timer3.Percent * 100f).ToString("0.0")}%");
        }
    }
}
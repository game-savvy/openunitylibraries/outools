using UnityEngine;

namespace GameSavvy.OpenUnityTools.Timers
{
    internal class FunctionTimerController : MonoBehaviour
    {
        private void Update()
        {
            for (int i = FunctionTimer.ExistingTimers.Count - 1; i >= 0; i--)
            {
                var item = FunctionTimer.ExistingTimers[i];
                if (item.ShouldDestroy)
                {
                    FunctionTimer.ExistingTimers.RemoveAt(i);
                }
                else
                {
                    item.TimerTick(Time.deltaTime);
                }
            }
        }
    }
}
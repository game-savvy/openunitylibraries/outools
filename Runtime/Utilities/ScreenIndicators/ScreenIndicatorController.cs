﻿using System.Collections.Generic;
using UnityEngine;
using GameSavvy.OpenUnityTools.ObjectPooling;

namespace GameSavvy.OpenUnityTools.ScreenIndicators
{
    public class ScreenIndicatorController : MonoBehaviour
    {
        public static readonly List<ScreenTarget> ScreenTargets = new();

        [Range(0.5f, 0.9f)]
        [Tooltip("Distance offset of the indicators from the centre of the screen")]
        [SerializeField]
        private float _screenBoundOffset = 0.9f;

        [SerializeField]
        private int _defaultCapacity = 8;

        [SerializeField]
        private int _maxCapacity = 16;


        [Header("Indicator Prefabs")]
        [SerializeField]
        private ArrowIndicator _arrowScreenIndicatorPrefab;

        [SerializeField]
        private BoxIndicator _boxScreenIndicatorPrefab;

        private Camera _mainCamera;
        private Vector3 _screenCentre;
        private Vector3 _screenBounds;

        private void Awake()
        {
            BehaviourPoolManager.CreatePool(_arrowScreenIndicatorPrefab, true, _defaultCapacity, _maxCapacity, transform);
            BehaviourPoolManager.CreatePool(_boxScreenIndicatorPrefab, true, _defaultCapacity, _maxCapacity, transform);

            _mainCamera = Camera.main;
        }

        private void LateUpdate()
        {
            _screenCentre = new Vector3(Screen.width, Screen.height, 0) / 2;
            _screenBounds = _screenCentre * _screenBoundOffset;
            DrawIndicators();
        }

        private void DrawIndicators()
        {
            foreach (var target in ScreenTargets)
            {
                var screenPosition = _mainCamera.WorldToScreenPoint(target.Position);
                var isTargetVisible = IsInScreen(screenPosition);

                ScreenIndicator indicator = null;

                if (target.NeedBoxIndicator && isTargetVisible)
                {
                    var arrowIndicator = target.ArrowIndicator;
                    BehaviourPoolManager.Release(arrowIndicator);
                    target.ArrowIndicator = null;

                    screenPosition.z = 0;
                    indicator = target.BoxIndicator;
                    if (indicator == null)
                    {
                        indicator = BehaviourPoolManager.Get(_boxScreenIndicatorPrefab, screenPosition, Quaternion.identity, transform);
                        target.BoxIndicator = indicator as BoxIndicator;
                    }
                    else
                    {
                        indicator.transform.position = screenPosition;
                    }
                }
                else if (target.NeedArrowIndicator && isTargetVisible == false)
                {
                    var boxIndicator = target.BoxIndicator;
                    BehaviourPoolManager.Release(boxIndicator);
                    target.BoxIndicator = null;

                    var angle = float.MinValue;
                    GetArrowIndicatorPositionAndAngle(ref screenPosition, ref angle, _screenCentre, _screenBounds);
                    var rotation = Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg); // Sets the rotation for the arrow indicator.
                    indicator = target.ArrowIndicator;
                    if (indicator == null)
                    {
                        indicator = BehaviourPoolManager.Get(_arrowScreenIndicatorPrefab, screenPosition, rotation, transform);
                        target.ArrowIndicator = indicator as ArrowIndicator;
                    }
                    else
                    {
                        indicator.transform.SetPositionAndRotation(screenPosition, rotation);
                    }
                }

                if (target.NeedDistanceText == false) continue;

                float distanceFromCamera = target.NeedDistanceText ? Vector3.Distance(_mainCamera.transform.position, target.Position) : 0f;
                indicator.SetDistanceText(distanceFromCamera);
                indicator.SetTextRotation(Quaternion.identity);
            }
        }

        private bool IsInScreen(Vector3 position)
        {
            return position.z > 0
                && position.x > 0
                && position.x < Screen.width
                && position.y > 0
                && position.y < Screen.height;
        }

        private void GetArrowIndicatorPositionAndAngle(ref Vector3 screenPosition, ref float angle, Vector3 screenCentre, Vector3 screenBounds)
        {
            // Bring the ScreenPosition to the viewable screen space.
            screenPosition -= screenCentre;

            // projections of Targets behind the camera are inverted, fix them
            if (screenPosition.z < 0)
            {
                screenPosition *= -1;
            }

            angle = Mathf.Atan2(screenPosition.y, screenPosition.x);
            float slope = Mathf.Tan(angle); // Slope of the line starting from zero and terminating at screenPosition.

            // y = slope * x.
            screenPosition = screenPosition.x > 0 ?
                                 new Vector3(screenBounds.x, screenBounds.x * slope, 0) :
                                 new Vector3(-screenBounds.x, -screenBounds.x * slope, 0);

            // In case the y ScreenPosition exceeds the y screenBounds 
            if (screenPosition.y > screenBounds.y)
            {
                screenPosition = new Vector3(screenBounds.y / slope, screenBounds.y, 0);
            }
            else if (screenPosition.y < -screenBounds.y)
            {
                screenPosition = new Vector3(-screenBounds.y / slope, -screenBounds.y, 0);
            }

            // Bring the ScreenPosition back to the original position.
            screenPosition += screenCentre;
        }
    }
}
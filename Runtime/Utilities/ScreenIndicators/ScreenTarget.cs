﻿using UnityEngine;
using GameSavvy.OpenUnityTools.ObjectPooling;

namespace GameSavvy.OpenUnityTools.ScreenIndicators
{
    /// <summary>
    /// Attach this script to all the target game objects in the scene.
    /// </summary>
    public class ScreenTarget : MonoBehaviour
    {
        [Tooltip("Change this color to change the indicators color for this target")]
        [SerializeField]
        private Color _targetColor = Color.red;

        [Tooltip("Select if box indicator is required for this target")]
        [SerializeField]
        private bool _needBoxIndicator = true;

        [Tooltip("Select if arrow indicator is required for this target")]
        [SerializeField]
        private bool _needArrowIndicator = true;

        [Tooltip("Select if distance text is required for this target")]
        [SerializeField]
        private bool _needDistanceText = true;

        private Transform _transform;

        private BoxIndicator _boxIndicator;
        public BoxIndicator BoxIndicator
        {
            get => _needBoxIndicator == false ? null : _boxIndicator;
            set
            {
                _boxIndicator = value;
                _boxIndicator?.SetImageColor(_targetColor);
            }
        }

        private ArrowIndicator _arrowIndicator;
        public ArrowIndicator ArrowIndicator
        {
            get => _needArrowIndicator == false ? null : _arrowIndicator;
            set
            {
                _arrowIndicator = value;
                _arrowIndicator?.SetImageColor(_targetColor);
            }
        }

        public bool NeedBoxIndicator => _needBoxIndicator;
        public bool NeedArrowIndicator => _needArrowIndicator;
        public bool NeedDistanceText => _needDistanceText;
        public Vector3 Position => _transform.position;

        private void Awake()
        {
            _transform = transform;
        }

        private void OnEnable()
        {
            ScreenIndicatorController.ScreenTargets.Add(this);
        }

        private void OnDisable()
        {
            BehaviourPoolManager.Release(_arrowIndicator);
            BehaviourPoolManager.Release(_boxIndicator);
            ScreenIndicatorController.ScreenTargets.Remove(this);
        }
    }
}
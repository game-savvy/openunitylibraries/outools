﻿using UnityEngine;
using UnityEngine.UI;

namespace GameSavvy.OpenUnityTools.ScreenIndicators
{
    /// <summary>
    /// Assign this script to the indicator prefabs.
    /// </summary>
    public abstract class ScreenIndicator : MonoBehaviour
    {
        protected Image _indicatorImage;
        protected Text _distanceText;

        protected void Awake()
        {
            _indicatorImage = GetComponent<Image>();
            _distanceText = GetComponentInChildren<Text>();
        }

        public void SetImageColor(Color color) => _indicatorImage.color = color;
        public void SetDistanceText(float value) => _distanceText.text = value >= 0 ? $"{Mathf.Floor(value)} m" : "";
        public void SetTextRotation(Quaternion rotation) => _distanceText.rectTransform.rotation = rotation;
    }
}
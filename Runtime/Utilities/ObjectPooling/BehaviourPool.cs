using UnityEngine;
using UnityEngine.Pool;

namespace GameSavvy.OpenUnityTools.ObjectPooling
{
    public class BehaviourPool<T> where T : MonoBehaviour
    {
        private readonly ObjectPool<T> _pool;
        private readonly T _prefab;
        private readonly Transform _parent;

        public int CountAll => _pool.CountAll;
        public int CountActive => _pool.CountActive;
        public int CountInactive => _pool.CountInactive;

        public BehaviourPool(T prefab, bool collectionCheck, int defaultCapacity, int maxCapacity, Transform parent = null)
        {
            _prefab = prefab;
            _parent = parent;

            _pool = new(CreateInstance, GetInstance, ReleaseInstance, DestroyInstance, collectionCheck, defaultCapacity, maxCapacity);
        }

        protected virtual T CreateInstance()
        {
            T instance = GameObject.Instantiate(_prefab, _parent);
            return instance;
        }

        // keep empty since we manage things in the ```T Get``` function
        protected virtual void GetInstance(T instance)
        {
        }

        protected virtual void ReleaseInstance(T instance)
        {
            instance.gameObject.SetActive(false);
        }

        protected virtual void DestroyInstance(T instance)
        {
            GameObject.Destroy(instance.gameObject);
        }

        public virtual T Get(Vector3 position, Quaternion rotation, Transform parent = null)
        {
            T instance = _pool.Get();
            instance.transform.SetPositionAndRotation(position, rotation);
            instance.transform.SetParent(parent);
            instance.gameObject.SetActive(true);
            return instance;
        }

        public virtual void Release(T instance)
        {
            if (instance == null) return;

            _pool.Release(instance);
        }
    }
}
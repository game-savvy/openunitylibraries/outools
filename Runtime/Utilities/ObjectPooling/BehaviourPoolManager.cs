using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameSavvy.OpenUnityTools.ObjectPooling
{
    public static class BehaviourPoolManager
    {
        private const int _defaultCapacity = 8;
        private const int _maxCapacity = 10000;
        private static Dictionary<Type, BehaviourPool<MonoBehaviour>> _pools = new();

        public static void CreatePool<T>(T prefab, bool collectionCheck, int defaultCapacity, int maxCapacity, Transform parent = null)
            where T : MonoBehaviour
        {
            if (prefab == null) return;

            Type type = typeof(T);
            if (_pools.ContainsKey(type)) return;

            _pools[type] = new(prefab, collectionCheck, defaultCapacity, maxCapacity, parent);
        }

        public static T Get<T>(T prefab, Vector3 pos, Quaternion rot, Transform parent) where T : MonoBehaviour
        {
            if (prefab == null) return null;

            Type type = typeof(T);
            if (_pools.ContainsKey(type) == false) CreatePool(prefab, false, _defaultCapacity, _maxCapacity);

            return _pools[type].Get(pos, rot, parent) as T;
        }

        public static void Release<T>(T instance) where T : MonoBehaviour
        {
            if (instance == null) return;

            Type type = typeof(T);
            if (_pools.ContainsKey(type) == false) return;

            _pools[type].Release(instance);
        }
    }
}
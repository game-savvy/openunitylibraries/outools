# OUTools

*Open Unity Tools - Under Development*

An Open Source library for the Unity Engine.

Open Unity Tools, is a library with a compilation of tools, extensions and utilities for extending and making Unity Development Easier and faster.
  
___
## Dependencies and Requirements

Unity 2020.3 or later versions.

This library depends on:  
- You must have **GIT** installed in order to be able to download the package automatically.
- You must have InputSystem installed and enabled in your Unity Project
  
Please install all the dependecies before installing this library.
___
## How to Install

### **With the GitUrl**
1. First, open the **Package Manager Window**  
![Open Package Manager](https://gitlab.com/wcampospro/ouattributes/raw/61f1ccabebefa2c9ed8550455a42668dcdadf96d/Documentation~/Images/Unity_PackageManagerWindow.png)

2. Second, on the top left **+** sign, select Add Package from git URL...  
![PackageManager Add from Git URL](https://gitlab.com/wcampospro/ouattributes/raw/61f1ccabebefa2c9ed8550455a42668dcdadf96d/Documentation~/Images/PackageManager_GitURL.png)

3. Third, paste The ***HTTPS GitURL*** in the text field

The library is ready to be used...  
  
### **Through the Manifest.json**
You can also install via git url by adding this entry in your **manifest.json**
```
"com.game-savvy.outools": "https://gitlab.com/game-savvy/openunitylibraries/outools.git"
```
After this , you will be able to find the Package in the **Package Manager** window.

---
## **The future of Open Unity Tools**
There are many things planned to come to this library, including:
- Offscreen Target Visualizer
- FPS Controller
- TPS Controller
- VGS Menu System
- and much more...

The plan is to eventually also add Unit Testing and Examples per Feature added.

If you have anything that you would like to add/see/change in this library, please feel free to contribute to it.  
You can also create an Issue to make a *Feature Request* or a *Bug Report*.
  
---
## Want to Help? How to contribute to this Repo

For all code, please follow these [Unity Coding Standards and Naming Conventions][LinkUnityCodingStandards].  
Simply work on your fork, create a feature branch and when done please submit a pull request.
  
---
## License
MIT License

Copyright (c) 2020 Willy Campos

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[LinkOUTools]: https://gitlab.com/game-savvy/openunitylibraries/outools.git
[LinkOUScriptableLibrary]: https://gitlab.com/game-savvy/openunitylibraries/ousl.git
[LinkUnityCodingStandards]: https://gitlab.com/wcampospro/CodingConvention_Unity

using System;
using UnityEngine;
using UnityEditor;

namespace GameSavvy.OpenUnityTools.EditorTools
{
    public class ReplaceWithPrefab : EditorWindow
    {
        [SerializeField]
        private GameObject _prefab;

        private bool _applyRotation = true;
        private bool _applyScale = true;

        [MenuItem("Tools/Replace With Prefab")]
        public static void CreateReplaceWithPrefab()
        {
            GetWindow<ReplaceWithPrefab>().titleContent = new("Replace With Prefab");
        }

        private void OnGUI()
        {
            _prefab = (GameObject) EditorGUILayout.ObjectField("Prefab", _prefab, typeof(GameObject), false);
            var selectedObjects = Selection.gameObjects ?? Array.Empty<GameObject>();

            _applyRotation = GUILayout.Toggle(_applyRotation, "Apply Rotation");
            _applyScale = GUILayout.Toggle(_applyScale, "Apply Scale");

            GUI.enabled = _prefab != null && selectedObjects.Length > 0;
            if (GUILayout.Button("Replace"))
            {
                foreach (var objectToReplace in selectedObjects)
                {
                    var newObject = Instantiate(_prefab,
                                                objectToReplace.transform.localPosition,
                                                _applyRotation ? objectToReplace.transform.localRotation : Quaternion.identity,
                                                objectToReplace.transform.parent);
                    Undo.RegisterCreatedObjectUndo(newObject, "Replace With Prefabs");
                    if (_applyScale) newObject.transform.localScale = objectToReplace.transform.localScale;
                    newObject.name = _prefab.name;
                    newObject.transform.SetSiblingIndex(objectToReplace.transform.GetSiblingIndex());

                    Undo.DestroyObjectImmediate(objectToReplace);
                }
            }

            GUI.enabled = false;
            EditorGUILayout.LabelField($"Selection count: {Selection.objects.Length}");
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Select Object to replace, and prefab to replace with.");
            EditorGUILayout.LabelField("All operations are undoable.");
        }
    }
}
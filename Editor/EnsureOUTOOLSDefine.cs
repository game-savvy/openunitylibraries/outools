﻿#if OU_TOOLS
#else
#define OU_TOOLS
#endif

#if UNITY_EDITOR

namespace GameSavvy.ScriptableLibrary
{
    using System;
    using System.Linq;
    using UnityEditor;

    internal static class EnsureOUTOOLSDefine
    {
        private static readonly string[] DEFINES = new string[] { "OU_TOOLS" };

        [InitializeOnLoadMethod]
        private static void EnsureScriptingDefineSymbol()
        {
            var currentTarget = EditorUserBuildSettings.selectedBuildTargetGroup;

            if (currentTarget == BuildTargetGroup.Unknown)
            {
                return;
            }

            var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(currentTarget).Trim();
            var defines = definesString.Split(';');

            bool changed = false;

            foreach (var define in DEFINES)
            {
                if (defines.Contains(define) == false)
                {
                    if (definesString.EndsWith(";", StringComparison.InvariantCulture) == false)
                    {
                        definesString += ";";
                    }

                    definesString += define;
                    changed = true;
                }
            }

            if (changed)
            {
                PlayerSettings.SetScriptingDefineSymbolsForGroup(currentTarget, definesString);
            }
        }
    }
}

#endif // UNITY_EDITOR

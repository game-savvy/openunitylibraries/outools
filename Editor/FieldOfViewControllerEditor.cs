using UnityEngine;
using UnityEditor;

namespace GameSavvy.OpenUnityTools.Extensions
{
    [CustomEditor(typeof(FieldOfViewController))]
    public class FieldOfViewControllerEditor : Editor
    {
        private void OnSceneGUI()
        {
            FieldOfViewController fov = (FieldOfViewController)target;

            Vector3 pos = fov.transform.position + Vector3.up * fov.VerticalGizmoOffset;

            Handles.color = fov.WireColor;
            Handles.DrawWireArc(pos, Vector3.up, fov.transform.forward, 360f, fov.Radius);
            Handles.DrawSolidArc(pos, Vector3.up, fov.transform.forward, fov.Angle / 2f, fov.Radius);
            Handles.DrawSolidArc(pos, Vector3.up, fov.transform.forward, -fov.Angle / 2f, fov.Radius);

            if (fov.Targets == null) return;
            Handles.color = fov.TargetColor;
            foreach (var t in fov.Targets)
            {
                Handles.DrawLine(pos, t.position.With(y: pos.y));
            }
        }
    }
}
